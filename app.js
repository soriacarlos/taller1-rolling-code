const form = document.querySelector('#formOff');

form.addEventListener("submit", function (e) {
  e.preventDefault();

  let precio = 0;
  let descuento = 0;
  precio = form.querySelector('#price').value;
  descuento = form.querySelector('#desc').value;
  cuponRequest = form.querySelector('#cupon').value;
  cupon = cuponRequest.trim();
  const formula = (precio * (100 - descuento)) / 100;

  if (precio > 0 && !cupon) {
    return document.querySelector('#result').innerHTML = `El precio del producto ${(descuento != '0') ? `con descuento del ${descuento}%` : 'sin descuento'} es de $${formula.toFixed(2)}`;
  }

  if (precio > 0 && cupon == 'PROMOMARZO2022') {

    let cuponDescuento = (formula * 55) / 100;
    let precioConCupon = formula - cuponDescuento;

    return document.querySelector('#result').innerHTML = `El precio del producto ${(descuento != '0') ? `con descuento del ${descuento}%` : 'sin descuento'} y cupón 'PROMOMARZO2022' es de $${precioConCupon.toFixed(2)}`;
  }

  if (precio > 0 && cupon == 'CYBERMONDAY2022') {

    let cuponDescuento = (formula * 70) / 100;
    let precioConCupon = formula - cuponDescuento;

    return document.querySelector('#result').innerHTML = `El precio del producto ${(descuento != '0') ? `con descuento del ${descuento}%` : 'sin descuento'} y cupón 'CYBERMONDAY2022' es de $${precioConCupon.toFixed(2)}`;
  }

  if (precio > 0 && cupon == 'BLACKFRIDAY2022') {

    let cuponDescuento = (formula * 95) / 100;
    let precioConCupon = formula - cuponDescuento;

    return document.querySelector('#result').innerHTML = `El precio del producto ${(descuento != '0') ? `con descuento del ${descuento}%` : 'sin descuento'} y cupón 'BLACKFRIDAY2022' es de $${precioConCupon.toFixed(2)}`;
  }

  return document.querySelector('#result').innerHTML = `Debe ingresar un valor númerico válido`;

}, true);
